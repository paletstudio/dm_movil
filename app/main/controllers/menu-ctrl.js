(function() {
	'use strict';

	angular
		.module('main')
		.controller('MenuCtrl', MenuCtrl);

	MenuCtrl.$inject = [
		'$scope',
		'sessionService',
		'cameraService',
		'Config',
		'userService',
		'cloudNotificationService',
		'$window',
        '$ionicPlatform',
        'utilsService'
	];

	function MenuCtrl($scope, sessionService, cameraService, Config, userService, cloudNotificationService, $window, $ionicPlatform, utilsService) {

		var menu = this;
		menu.logout = logout;
		setUserLoggedIn();
		menu.url = Config.ENV.API_URL;
		menu.prof_pic = menu.url + 'user/' + menu.user.id + '/picture?' + moment().unix();
		menu.changeProfilePicture = changeProfilePicture;

		function logout() {
			sessionService.logout();
		}

		function setUserLoggedIn() {
			menu.user = userService.getUser();
		}

		sessionService.onLoginEvent(function() {
			setUserLoggedIn();
			menu.prof_pic = menu.url + 'user/' + menu.user.id + '/picture?' + moment().unix();
		});

		function changeProfilePicture() {
			return cameraService.getPicture()
				.then(function(res) {
					userService.changeProfilePicture(menu.user.id, res)
						.then(function() {
							menu.prof_pic = menu.url + 'user/' + menu.user.id + '/picture?' + moment().unix();
						});
				});
		}

		// $scope.$on('cloud:push:notification', function(event, data) {
		// 	cloudNotificationService.dispatch(data);
		// });

        $ionicPlatform.ready(function() {
            $window.addEventListener('online', function() {
                utilsService.hideLoading();
            }, false);

            $window.addEventListener('offline', function() {
                utilsService.showLoading('Parece que no tienes conexión a Internet. Intentando conectar...');
            }, false);
        });


	}

})();
