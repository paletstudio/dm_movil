(function() {
	'use strict';

	angular.module('main')
		.config(config);

	config.$inject = [
		'$stateProvider',
		'$urlRouterProvider',
		'$ionicConfigProvider',
		'$ionicCloudProvider',
		'lockerProvider',
		'jwtOptionsProvider',
		'$httpProvider',
	];

	function config($stateProvider, $urlRouterProvider, $ionicConfigProvider, $ionicCloudProvider, lockerProvider, jwtOptionsProvider, $httpProvider) {
		String.prototype.replaceAll = function(search, replacement) {
			var target = this;
			return target.split(search).join(replacement);
		};

		if (!ionic.Platform.isIOS()) {
			$ionicConfigProvider.scrolling.jsScrolling(false);
		}
		$ionicConfigProvider.backButton.text('').previousTitleText(false);
        $ionicConfigProvider.views.swipeBackEnabled(false);

		$ionicCloudProvider.init({
			'core': {
				'app_id': '81b11f8d'
			},
			'push': {
				'sender_id': '193591149512',
				'pluginConfig': {
					'ios': {
                        'clearBadge': true,
						'badge': true,
						'sound': true
					},
					'android': {
						'iconColor': '#343434'
					}
				}
			}
		});

		lockerProvider.defaults({
			driver: 'local',
			namespace: 'dm',
			separator: '_',
			eventsEnabled: true,
			extend: {}
		});
		// ROUTING with ui.router
		$urlRouterProvider.otherwise('/main/course/1');
		//$urlRouterProvider.otherwise('/main/course/1');
		$stateProvider
			// this state is placed in the <ion-nav-view> in the index.html
			.state('main', {
				url: '/main',
				abstract: true,
				templateUrl: 'main/templates/menu.html',
				controller: 'MenuCtrl as menu'
			});

		jwtOptionsProvider.config({
			whiteListedDomains: ['dm.cobusiness.mx', '158.69.202.20', '192.168.10.10'],
			tokenGetter: [
				'options',
				'sessionService',
				'jwtHelper',
				'API',
				'storageService',
				function(options, sessionService, jwtHelper, API, storageService) {
					if (!options || options.url.substr(options.url.length - 5) === '.html') {
						return null;
					}
					return sessionService.getToken()
						.then(function(jwt) {
							if (jwt && jwtHelper.isTokenExpired(jwt)) {
								return API.get('refreshtoken', {
										skipAuthorization: true,
										headers: {
											Authorization: 'Bearer ' + jwt
										}
									})
									.then(function(response) {
											storageService.put('token', response.data.data);
											return response.data.data;
										},
										function(response) {
											sessionService.cleanData();
										});
							} else {
								return jwt;
							}
						});
					//return locker.get('token');
				}
			],
			unauthenticatedRedirector: ['sessionService', function(sessionService) {
				sessionService.isLoggedIn()
					.then(function(value) {
						if (value) {
							sessionService.logout();
						}
					});
			}]
		});
		$httpProvider.interceptors.push('jwtInterceptor');
	}
})();
