(function() {
	'use strict';

	angular
		.module('main')
		.run(appRun);

	appRun.$inject = ['routerHelper'];
	/* @ngInject */
	function appRun(routerHelper) {
		routerHelper.configureStates(getStates());
	}

	function getStates() {
		var baseUrl = 'main/components/user/';

		return [{
			state: 'main.user',
			config: {
				url: '/user',
				views: {
					'pageContent@main': {
						templateUrl: baseUrl + 'user.html',
						controller: 'UserController as vm'
					}
				},
				data: {}
			}
		}];
	}
})();
