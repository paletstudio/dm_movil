(function() {
	'use strict';

	angular
		.module('main')
		.controller('UserController', UserController);

	UserController.$inject = ['userService', 'utilsService'];

	/* @ngInject */
	function UserController(userService, utilsService) {
		var vm = this;
		vm.changePassword = changePassword;

		activate();

		function activate() {
			vm.user = userService.getUser();
		}

		function changePassword() {
			utilsService.showLoading();
			return userService.changePassword(vm.user.id, {
					password: vm.user.password,
					new_password: vm.user.new_password
				})
				.then(function(res) {
					utilsService.showAlertDialog(res.msg)
						.then(function() {
							vm.user.password = null;
							vm.user.new_password = null;
							vm.user.renew_password = null;
						});
				})
				.catch(function(err) {
					utilsService.showAlertDialog(err);
				})
				.finally(function() {
					utilsService.hideLoading();
				});
		}
	}
})();
