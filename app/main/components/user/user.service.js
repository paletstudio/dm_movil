(function() {
	'use strict';

	angular
		.module('main')
		.service('userService', userService);

	userService.$inject = ['API'];

	/* @ngInject */
	function userService(API) {
		var me = this;
		var _user = null;

		me.setUser = function(user) {
			_user = user;
		};

		me.getUser = function() {
			return _user || null;
		};

		me.changeProfilePicture = function(id, image) {
			return API.post('user/' + id + '/picture', {
					profile_pic: image
				})
				.then(function(res) {
					return res.data;
				})
				.catch(function(err) {
					return err;
				});
		};

		me.changePassword = function(id, body) {
			return API.patch('user/' + id + '/password', body)
				.then(function(res) {
					return res.data;
				});
		};
	}
})();
