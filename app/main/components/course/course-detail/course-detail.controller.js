(function() {
	'use strict';

	angular
		.module('main')
		.controller('CourseDetailController', CourseDetailController);

	CourseDetailController.$inject = ['$scope', '$state', '$q', 'programService'];

	/* @ngInject */
	function CourseDetailController($scope, $state, $q, programService) {
		var vm = this;
		vm.course = null;
		vm.blocks = [];
		vm.goNextLevel = goNextLevel;
		vm.shouldAnimate = true;

		$scope.$on('$ionicView.beforeEnter', activate);

		function activate() {
			if (vm.blocks.length > 0) {
				vm.shouldAnimate = false;
			}
			setCurrent();
			getData();
		}

		function getData() {
			var promises = {
				//	course: programService.getCourse(),
				blocks: programService.getBlocks()
			};

			return $q.all(promises)
				.then(function(res) {
					vm.course = res.course;
					vm.blocks = res.blocks;
				});
		}

		function setCurrent() {
			programService.currentCourse = $state.params.course_id;
		}

		function goNextLevel(item) {
			if (item.access && item.access.locked === 0) {
				//Logica para brincar pantalla de modulos
				//Quitar para futuros proyectos
				if (item.id === 1) {
					$state.go('.block.module', {
						block_id: item.id,
						module_id: 1
					});
					return;
				} else if (item.id === 4) {
					$state.go('.block.module', {
						block_id: item.id,
						module_id: 14
					});
					return;
				}
				//Termina logica
				$state.go('.block', {
					block_id: item.id
				});
			}
		}

		$scope.$on('cloud:push:notification', function(event, data) {
			activate();
		});

	}
})();
