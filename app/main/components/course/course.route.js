(function() {
	'use strict';

	angular
		.module('main')
		.run(appRun);

	appRun.$inject = ['routerHelper'];
	/* @ngInject */
	function appRun(routerHelper) {
		routerHelper.configureStates(getStates());
	}

	function getStates() {
		var baseUrl = 'main/components/course/';

		return [{
			state: 'main.course',
			config: {
				url: '/course',
				views: {
					'pageContent': {
						templateUrl: baseUrl + 'course.html',
						controller: 'CourseController as vm'
					}
				},
				data: {}
			}
		}, {
			state: 'main.course.detail',
			config: {
				url: '/{course_id:int}',
				views: {
					'pageContent@main': {
						templateUrl: baseUrl + 'course-detail/course-detail.html',
						controller: 'CourseDetailController as vm'
					}
				},
				data: {

				},
				resolve: {
					resolve: ['programService', '$stateParams', function(programService, $stateParams) {
						programService.currentCourse = $stateParams.course_id;
					}]
				}
			}
		}];
	}
})();
