(function() {
	'use strict';

	angular
		.module('main')
		.run(appRun);

	appRun.$inject = ['routerHelper'];
	/* @ngInject */
	function appRun(routerHelper) {
		routerHelper.configureStates(getStates());
	}

	function getStates() {
		var baseUrl = 'main/components/block/';

		return [{
			state: 'main.course.detail.block',
			config: {
				url: '/block/{block_id:int}',
				views: {
					'pageContent@main': {
						templateUrl: baseUrl + 'block.html',
						controller: 'BlockController as vm'
					}
				},
				data: {},
				resolve: {
					resolve: ['programService', '$stateParams', function(programService, $stateParams) {
						programService.currentBlock = $stateParams.block_id;
					}]
				}
			}
		}];
	}
})();
