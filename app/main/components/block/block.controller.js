(function() {
	'use strict';

	angular
		.module('main')
		.controller('BlockController', BlockController);

	BlockController.$inject = ['$scope', '$state', '$q', 'programService'];

	/* @ngInject */
	function BlockController($scope, $state, $q, programService) {
		var vm = this;
		vm.block = null;
		vm.modules = null;
		vm.goNextLevel = goNextLevel;

		$scope.$on('$ionicView.beforeEnter', activate);

		function activate() {
			setCurrent();
			getData();
		}

		function getData() {
			var promises = {
				//	course: programService.getCourse(),
				block: programService.getBlock(),
				modules: programService.getModules()
			};

			return $q.all(promises)
				.then(function(res) {
					vm.block = res.block;
					vm.modules = res.modules;
				});
		}

		function setCurrent() {
			programService.currentBlock = $state.params.block_id;
		}

		function goNextLevel(item, last) {
			if (item.access && item.access.locked === 0) {
				if (last) {
					$state.go('.module', {
						module_id: item.id,
						lastModule: last
					});
					return;
				}
				$state.go('.module', {
					module_id: item.id
				});

			}
		}
	}
})();
