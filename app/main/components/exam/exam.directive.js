(function() {
	'use strict';

	angular
		.module('main')
		.directive('exam', exam);

	/* @ngInject */
	function exam() {
		var directive = {
			restrict: 'A',
			scope: {
				content: '<',
				challenge: '='
			},
			controller: ExamController,
			controllerAs: 'vm',
			bindToController: true
		};

		return directive;
	}

	ExamController.$inject = [
		'$scope',
		'$element',
		'$state',
		'$ionicModal',
		'$ionicHistory',
		'programService',
		'utilsService',
		'cameraService',
		'videoService',
		'userService',
		'$timeout'
	];

	/* @ngInject */
	function ExamController($scope, $element, $state, $ionicModal, $ionicHistory, programService, utilsService, cameraService, videoService, userService, $timeout) {
		var vm = this;
		var examId = null;
		var videos = [];
		var user = null;
		vm.closeModal = closeModal;
		vm.currentQuestion = 0;
		vm.answerQuestion = answerQuestion;
		vm.examIsFinished = false;
		vm.saveExam = saveExam;
		vm.nextModule = nextModule;
		vm.examSent = false;
		vm.takePhoto = takePhoto;
		vm.takeVideo = takeVideo;
		vm.video = null;
		vm.isLastCourseModule = false;
        vm.countCorrectAnswers = countCorrectAnswers;
		vm.validateAnswer = validateAnswer;

		$ionicModal.fromTemplateUrl('main/components/exam/exam.html', {
				scope: $scope
			})
			.then(function(modal) {
				vm.modal = modal;
			});

		$element.on('click', function() {
			programService.currentContent = vm.content.id;
			getExam();
			setCurrentUser();
			vm.modal.show();

			vm.isLastCourseModule = programService.checkIfLastCourseModule($state.params.course_id, $state.params.block_id, $state.params.module_id);
		});

		function answerQuestion(answer, $event) {
			vm.clickedAnswer = answer;
			vm.correctAnswer = vm.content.exam[vm.currentQuestion].correct_answer_id;
			utilsService.showLoading();

			$timeout(function() {
					vm.content.exam[vm.currentQuestion].answer = answer;
					checkIfFinishedExam();
					vm.currentQuestion += 1;
					vm.image = null;
					vm.video = null;
					vm.openedAnswer = null;

					utilsService.hideLoading();
				}, 2000)
				.then(function() {
					vm.clickedAnswer = null;
					vm.correctAnswer = null;
				});
		}

		function getExam() {
			programService.getContent()
				.then(function(res) {
					vm.content = resetAnswers(res);
				});
		}

		function saveExam() {
			var examAttempt = {
				user_id: user.id,
				questions: vm.content.exam
			};

			utilsService.showLoading('Enviando quiz...');

			//si tiene video llamar ruta para guardar video
			if (hasVideos()) {
				console.log(videos);
				return videoService.uploadVideo(examAttempt.user_id, videos[0].questionId, videos[0].path)
					.then(function(res) {
						if (res) {
							vm.examSent = true;
							// utilsService.showAlertDialog(res.msg);
						}
					})
					.catch(function(err) {
						console.log(err);
					})
					.finally(function() {
						utilsService.hideLoading();
					});
			} else {
				return programService.saveExam(examAttempt)
					.then(function(res) {
						if (res.data) {
							vm.examSent = true;
							if (res.data.grade || res.data.grade === 0) {
								vm.grade = res.data.grade;
							}
							// utilsService.showAlertDialog(res.msg);
						}
					})
					.catch(function(err) {
						console.log(err);
					})
					.finally(function() {
						utilsService.hideLoading();
					});
			}

		}

		function nextModule() {
			closeModal()
				.then(function() {
					if ($state.params.lastModule) {
						$ionicHistory.goBack(-2);
						return;
					}
					$ionicHistory.goBack(-1);
				});
		}

		function checkIfFinishedExam() {
			vm.examIsFinished = vm.content.exam.every(function(e) {
				return e.answer !== null;
			});
		}

		function countCorrectAnswers() {
			return vm.content.exam.reduce(function(prev, current) {
				return prev + (parseInt(current.answer) === parseInt(current.correct_answer_id) ? 1 : 0);
			}, 0);
		}

		function resetAnswers(res) {
			res.exam = res.exam.map(function(q) {
				q.answer = null;
				return q;
			});
			return res;
		}

		function closeModal() {
			return vm.modal.hide()
				.then(function() {
					vm.examIsFinished = false;
					vm.examSent = false;
					vm.currentQuestion = 0;
					examId = null;
					vm.video = null;
					vm.grade = null;
					videos = [];
				});
		}

		function takePhoto() {
			return cameraService.getPicture()
				.then(function(res) {
					vm.image = res;
				});
		}

		function takeVideo() {
			return videoService.takeVideo()
				.then(function(res) {
					vm.video = res;
					videos.push({
						questionId: vm.content.exam[vm.currentQuestion].id,
						path: res
					});
				});
		}

		function hasVideos() {
			return vm.content.exam.some(function(e) {
				return e.exam_answer_type_id === 4;
			});
		}

		function setCurrentUser() {
			user = userService.getUser();
		}

		function validateAnswer() {
			programService.validateAnswer(vm.content.id, vm.content.exam[0].id).then( function(res) {
				vm.examSent = true;
				vm.examIsFinished = true;
			});
		}
	}
})();
