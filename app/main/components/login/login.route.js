(function() {
    'use strict';

    angular
        .module('main')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        var baseUrl = 'main/components/login/';

        return [{
            state: 'login',
            config: {
                url: '/login',
                templateUrl: baseUrl + 'login.html',
                controller: 'LoginController as vm'
            }
        }, {
            state: 'forgot',
            config: {
                url: '/forgot',
                templateUrl: baseUrl + 'forgot.html',
                controller: 'LoginController as vm'
            }
        }];
    }
})();
