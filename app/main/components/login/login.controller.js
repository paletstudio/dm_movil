(function() {
	'use strict';

	angular
		.module('main')
		.controller('LoginController', LoginController);

	LoginController.$inject = ['sessionService', '$ionicHistory', '$scope', '$window', 'Config', '$state'];

	/* @ngInject */
	function LoginController(sessionService, $ionicHistory, $scope, $window, Config, $state) {
		var vm = this;
		vm.login = login;
		vm.forgot = forgot;
		vm.year = moment().format('YYYY');
		vm.keyBoardOpened = false;
		vm.sentMessage = false;
		vm.goRegister = goRegister;

		$scope.$on('$ionicView.beforeEnter', activate);

		function activate() {
			$ionicHistory.clearHistory();
		}

		function login() {
			if (vm.username && vm.password) {
				return sessionService.login(vm.username, vm.password);
			}
		}

		function forgot() {
			sessionService.forgot(vm.username)
			.then(function() {
				vm.sentMessage = true;
			});
		}

		$window.addEventListener('native.keyboardshow', function() {
			$scope.$apply(function() {
				vm.keyBoardOpened = true;
			});
		});

		$window.addEventListener('native.keyboardhide', function() {
			$scope.$apply(function() {
				vm.keyBoardOpened = false;
			});
		});

		function goRegister() {
			$state.go('register');
		}
	}
})();
