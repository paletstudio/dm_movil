(function() {
    'use strict';

    angular
        .module('main')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        var baseUrl = 'main/components/home/';

        return [{
            state: 'main.home',
            config: {
                url: '/home',
                views: {
                    'pageContent': {
                        templateUrl: baseUrl + 'home.html',
                        controller: 'HomeController as vm'
                    }
                },
                data: {}
            }
        }];
    }
})();
