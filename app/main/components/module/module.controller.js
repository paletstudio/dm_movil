(function() {
	'use strict';

	angular
		.module('main')
		.controller('ModuleController', ModuleController);

	ModuleController.$inject = ['$state', '$q', 'programService'];

	/* @ngInject */
	function ModuleController($state, $q, programService) {
		var vm = this;
		vm.block = null;
		vm.modules = null;

		activate();

		function activate() {
			getData();
		}

		function getData() {
			var promises = {
				//	course: programService.getCourse(),
				module: programService.getModule(),
				contents: programService.getContents()
			};

			return $q.all(promises)
				.then(function(res) {
					vm.module = res.module;
					vm.contents = res.contents;
					checkUnlockBlock2();
				});
		}

		function checkUnlockBlock2() {
			if ($state.params.block_id === 1 && vm.module.order === 1) {
				programService.unlockAccess('block', 2);
				programService.doneAccess('block', 1);
			} else if ($state.params.block_id === 4 && vm.module.order === 1) {
				programService.doneAccess('block', 4);
			}
		}
	}
})();
