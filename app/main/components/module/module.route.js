(function() {
	'use strict';

	angular
		.module('main')
		.run(appRun);

	appRun.$inject = ['routerHelper'];
	/* @ngInject */
	function appRun(routerHelper) {
		routerHelper.configureStates(getStates());
	}

	function getStates() {
		var baseUrl = 'main/components/module/';

		return [{
			state: 'main.course.detail.block.module',
			config: {
				url: '/module/{module_id:int}?lastModule',
				views: {
					'pageContent@main': {
						templateUrl: baseUrl + 'module.html',
						controller: 'ModuleController as vm'
					}
				},
				data: {},
				resolve: {
					resolve: ['programService', '$stateParams', function(programService, $stateParams) {
						programService.currentModule = $stateParams.module_id;
					}]
				}
			}
		}];
	}
})();
