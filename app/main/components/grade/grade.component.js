(function() {
    'use strict';

    angular
        .module('main')
        .component('grade', grade());

    /* @ngInject */
    function grade() {
        var component = {
            templateUrl: 'main/components/grade/grade.html',
            controller: GradeController,
            controllerAs: 'vm',
            bindings: {
                value: '=',
                showFull: '=',
                showPending: '='
            }
        };

        return component;
    }

    GradeController.$inject = ['Config'];

    /* @ngInject */
    function GradeController(Config) {
        var vm = this;

        vm.$onInit = function() {
            if (vm.showPending === undefined) {
                vm.showPending = true;
            }
            if (Number(vm.value) === Config.ENV.GRADE.STAR) {
                vm.case = 1;
            } else if (Number(vm.value) > Config.ENV.GRADE.APPROVED) {
                vm.case = 2;
            } else if (Number(vm.value) >= Config.ENV.GRADE.INSUFFICIENT) {
                vm.case = 3;
            } else if (Number(vm.value) < Config.ENV.GRADE.INSUFFICIENT) {
                vm.case = 4;
            }
        };

    }
})();
