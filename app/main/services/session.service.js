(function() {
	'use strict';

	angular
		.module('main')
		.service('sessionService', sessionService);

	sessionService.$inject = [
		'$http',
		'$q',
		'API',
		'userService',
		'$state',
		'$rootScope',
		'jwtHelper',
		'storageService',
		'$ionicPush',
		'utilsService',
		'Config',
		'locker'
	];

	/* @ngInject */
	function sessionService($http, $q, API, userService, $state, $rootScope, jwtHelper, storageService, $ionicPush, utilsService, Config, locker) {
		var me = this;
		var pushToken = null;

		me.login = function(username, password) {
			utilsService.showLoading();

			return storageService.get('playerId').then(function(pushToken) {
				return API.post('login', {
						username: username,
						password: password,
						pushToken: pushToken,
						origin: Config.ENV.ORIGIN
					});
			})
				.then(function(res) {
					if (res.data && res.data.data) {
						var token = res.data.data;
						return storeSessionData(token, pushToken);
					} else {
						return $q.reject();
					}
				})
				.then(function() {
					fireLoginEvent();
					$state.go('main.course.detail', {
						course_id: 1
					});
				})
				.catch(function(err) {
					console.log(err);
					utilsService.showAlertDialog(err, 'Error');
				})
				.finally(function() {
					utilsService.hideLoading();
				});
		};

		me.logout = function() {
			utilsService.showLoading();

			return API.post('logout')
				.then(function(res) {
					if (res.data && res.data.data) {
						removeSessionData();
						fireLogoutEvent();

						$state.go('login');
					}
				})
				.catch(function(err) {
					removeSessionData();
					fireLogoutEvent();
					$state.go('login');
				})
				.finally(function() {
					utilsService.hideLoading();
				});
		};

		me.forgot = function(email) {
			return API.post('user/forgotpass', {email: email})
			.then(function(res) {
				return $q.resolve(res.data);
			});
		};

		me.cleanData = function() {
			utilsService.showLoading();
			removeSessionData();
			fireLogoutEvent();
			$state.go('login');
			utilsService.hideLoading();
		};

		me.isLoggedIn = function() {
			//var ret = false;
			return storageService.get('user')
				.then(function(value) {
					if (value) {
						userService.setUser(value);
						return $q.resolve(value);
					} else {
						return $q.resolve(false);
					}
				});
		};

		me.checkUpdatePushToken = function(token) {
			if (!cordovaIsDefined()) {
				return $q.reject();
			}

			return me.isLoggedIn()
				.then(function(res) {
					if (res) {
						return me.getPushToken();
					}
					return $q.reject('User is not logged in');
				})
				.then(function(res) {
					if (res) {
						//Si no hay token o es diferente del recibido en el parametro
						if (!res || (res !== token)) {
							return me.updatePushToken(token);
						}
						return $q.reject('Token is up to date');
					}
					return $q.reject('Push token could not be retrieved');
				});

		};

		// TODO: mover a userService ???
		me.updatePushToken = function(token) {
			var user = userService.getUser();
			if (!angular.isObject(user)) {
				return $q.reject('User is not an object');
			}
			return API.post('user/' + user.guid + '/pushtoken', {
					pushToken: token
				})
				.then(function(res) {
					if (res.data.data) {
						storageService.put('push_token', token);
					}
					return $q.resolve(res.data);
				})
				.catch(function(err) {
					console.log(err);
					return $q.reject(err);
				});
		};
		/**
		 * [onLogin this function will emit an event when a user has logged into the app]
		 * @return {[object]} [event object]
		 */
		me.onLoginEvent = function(fn) {
			return $rootScope.$on('login', fn);
		};

		me.onLogoutEvent = function(fn) {
			return $rootScope.$on('logout', fn);
		};

		me.getToken = function() {
			return storageService.get('token')
				.then(function(value) {
					return value;
				});
		};

		me.getPushToken = function() {
			return storageService.get('push_token')
				.then(function(value) {
					return value;
				});
		};

		function fireLoginEvent() {
			return $rootScope.$emit('login');
		}

		function fireLogoutEvent() {
			return $rootScope.$emit('logout');
		}

		function storeSessionData(token, pushToken) {
			var tokenPayload = jwtHelper.decodeToken(token);
			var user = tokenPayload.user;

			storageService.put('token', token);
			storageService.put('push_token', pushToken);

			return storageService.put('user', user)
				.then(function(value) {
					userService.setUser(user);
				});
		}

		function removeSessionData() {
			storageService.remove('token');
			storageService.remove('push_token');
			//storageService.remove('uma');
			storageService.remove('user');
		}

		function cordovaIsDefined() {
			return typeof Cordova !== 'undefined';
		}
	}
})();
