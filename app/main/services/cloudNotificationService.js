(function() {
	'use strict';

	angular
		.module('main')
		.service('cloudNotificationService', cloudNotificationService);

	cloudNotificationService.$inject = ['$state', '$rootScope', 'utilsService'];

	/* @ngInject */
	function cloudNotificationService($state, $rootScope, utilsService) {
		var me = this;

		me.notification = null;

		me.dispatch = function(data, origin) {
			console.log(data);
			console.log(origin);
			me.notification = data.notification || data;

			if (!(me.notification.payload.additionalData && me.notification.payload.additionalData.event)) {
				return false;
			}
			// triggerAction(me.notification.payload.additionalData.event);

			me.genericAction();

			// if (!(me.notification.message.payload && me.notification.message.payload.event)) {
			// 	return false;
			// }
			// triggerAction(me.notification.message.payload.event);
		};

		me.genericAction = function() {
			if (isInForeground()) {
				utilsService.showAlertDialog(me.notification.message.text, 'Draught Master');
			}
		};

		function isInForeground() {
			return me.notification.raw.additionalData.foreground;
		}

		function triggerAction(action) {
			if (typeof me[action] !== 'function') {
				return false;
			}
			me[action]();
		}

	}
})();
