(function() {
	'use strict';

	angular
		.module('main')
		.service('programService', programService);

	programService.$inject = ['API', '$q', 'userService'];

	/* @ngInject */
	function programService(API, $q, userService) {
		var me = this;
		me.currentCourse = null;
		me.currentBlock = null;
		me.currentModule = null;
		me.currentContent = null;

		me.getCourses = function() {

		};

		me.getCourse = function() {
			return API.get(getProgramUrl('course', true))
				.then(function(res) {
					return res.data.data;
				});
		};

		me.getBlocks = function() {
			return API.get(getProgramUrl('block'))
				.then(function(res) {
					return res.data.data;
				});
		};

		me.getBlock = function() {
			return API.get(getProgramUrl('block', true))
				.then(function(res) {
					return res.data.data;
				});
		};

		me.getModules = function() {
			return API.get(getProgramUrl('module'))
				.then(function(res) {
					return res.data.data;
				});
		};

		me.getModule = function() {
			return API.get(getProgramUrl('module', true))
				.then(function(res) {
					return res.data.data;
				});
		};

		me.getContents = function() {
			return API.get(getProgramUrl('content'))
				.then(function(res) {
					return res.data.data;
				});
		};

		me.getContent = function() {
			return API.get(getProgramUrl('content', true))
				.then(function(res) {
					return res.data.data;
				});
		};

		me.saveExam = function(exam) {
			return API.post(getProgramUrl('module', true) + '/exam', exam)
				.then(function(res) {
					return res.data;
				});
		};

		me.sendPDF2Email = function(body) {
			return API.post(getProgramUrl('content', true) + '/pdf', body)
				.then(function(res) {
					return res.data;
				});
		};

		me.checkIfLastCourseModule = function(courseId, blockId, moduleId) {
			return courseId === 1 && blockId === 3 && moduleId === 12;
		};

		me.unlockAccess = function(level, objectId) {
			return API.post(getProgramUrl(level) + '/' + objectId + '/unlock')
			.then(function(res) {
				return $q.resolve(res.data);
			});
		};

		me.doneAccess = function(level, objectId) {
			return API.post(getProgramUrl(level) + '/' + objectId + '/done')
			.then(function(res) {
				return $q.resolve(res.data);
			});
		};

		me.validateAnswer = function(contentId, questionId) {
			return API.post(getProgramUrl('content') + '/' + contentId + '/validate', {user_id: userService.getUser().id, question_id: questionId}).then(function(res) {
				return $q.resolve(res.data);
			});
		};

		me.getProgramUrl = getProgramUrl;

		function getProgramUrl(level, detail) {
			if (level === 'course') {
				return 'course' + (detail ? '/' + me.currentCourse : '');
			} else if (level === 'block') {
				return getProgramUrl('course', true) + '/block' + (detail ? '/' + me.currentBlock : '');
			} else if (level === 'module') {
				return getProgramUrl('block', true) + '/module' + (detail ? '/' + me.currentModule : '');
			} else if (level === 'content') {
				return getProgramUrl('module', true) + '/content' + (detail ? '/' + me.currentContent : '');
			} else {
				return '';
			}
		}
	}
})();
