(function() {
	'use strict';

	angular
		.module('main')
		.service('utilsService', utilsService);

	utilsService.$inject = ['$q', '$cordovaToast', '$cordovaDialogs', '$window', '$ionicLoading'];

	/* @ngInject */
	function utilsService($q, $cordovaToast, $cordovaDialogs, $window, $ionicLoading) {
		var me = this;

		me.showLoading = function(template) {
			return $ionicLoading.show({
				template: '<ion-spinner></ion-spinner> <br/> ' + (template || '')
			});
		};

		me.hideLoading = function() {
			return $ionicLoading.hide();
		};

		me.cordovaIsDefined = function() {
			return typeof Cordova !== 'undefined';
		};

		me.addQueryParam = function(path, param) {
			path += (path.split('?')[1] ? '&' : '?') + param;
			return path;
		};

		me.generateUUID = function() {
			var d = new Date().getTime();
			var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
				var r = (d + Math.random() * 16) % 16 | 0;
				d = Math.floor(d / 16);
				return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
			});
			return uuid;
		};

		me.showAlertDialog = function(message, title) {
			if (me.cordovaIsDefined()) {
				return $cordovaDialogs.alert(message, title);
			} else {
				$window.alert(message);
				return $q.when();
			}
		};

		me.showConfirmDialog = function(message, title) {
			if (me.cordovaIsDefined()) {
				return $cordovaDialogs.confirm(message, title)
					.then(function(res) {
						if (res === 1) {
							return $q.resolve(true);
						}
						return $q.reject(false);
					});
			}

			var confirm = $window.confirm(message);
			if (confirm) {
				return $q.resolve(true);
			}
			return $q.reject(false);
		};

		me.showToast = function(message, duration, position) {
			if (!(window.plugins && window.plugins.toast)) {
				console.log(message);
				return;
			}

			if (position === null || typeof position === 'undefined') {
				position = 'bottom';
			}

			return window.plugins.toast.show(message, duration, position);
		};

		me.requestCameraPermission = function() {
			var q = $q.defer();

			if (!(me.cordovaIsDefined())) {
				q.reject('Not a device');
				return q.promise;
			}
			if (!(cordova.plugins.diagnostic)) {
				q.reject('diagnostic plugin not installed');
			}
			var diagnostic = cordova.plugins.diagnostic;
			var errorCallback = function() {
				q.reject('Camera permission is not turned on');
			};
			diagnostic.isCameraAuthorized(function(status) {
				console.log(status);
				if (!status) {
					diagnostic.requestCameraAuthorization(function(status) {
							console.log(status);
							if (status !== diagnostic.permissionStatus.GRANTED) errorCallback();
						},
						errorCallback);
				}
				q.resolve(true);
			}, errorCallback);

			return q.promise;

		};
	}
})();
