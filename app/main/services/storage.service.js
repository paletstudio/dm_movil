(function() {
	'use strict';

	angular
		.module('main')
		.service('storageService', storageService);

	storageService.$inject = ['$q', 'locker', '$cordovaNativeStorage'];

	/* @ngInject */
	function storageService($q, locker, $cordovaNativeStorage) {
		var me = this;
        var prefix = 'dm_';

		me.get = function(key) {
			if (!angular.isString(key)) {
				key = key.toString();
			}
			if (cordovaIsDefined()) {
				return $cordovaNativeStorage.getItem(prefix + key)
					.then(function(value) {
						return JSON.parse(value);
					})
					.catch(function() {
						return null;
					});
			} else {
				return $q.resolve(locker.get(key) || null);
			}
		};

		me.put = function(key, value) {
			if (!angular.isString(key)) {
				key = key.toString();
			}
			if (value === null) {
				value = 'null';
			}
			if (cordovaIsDefined()) {
				return $cordovaNativeStorage.setItem(prefix + key, JSON.stringify(value))
					.then(function() {
						locker.put(key, value);
						return $q.resolve(value);
					});
			} else {
				locker.put(key, value);
				return $q.resolve(true);
			}
		};

		me.remove = function(key) {
			if (cordovaIsDefined()) {
				return $cordovaNativeStorage.remove(prefix + key)
					.then(function() {
						return $q.resolve(true);
					});
			} else {
				locker.forget(key);
				return $q.resolve(true);
			}
		};

		me.clear = function() {
			if (cordovaIsDefined()) {
                locker.clean();
				return $cordovaNativeStorage.clear();
			} else {
				return $q.resolve(true);
			}
		};

		function cordovaIsDefined() {
			return typeof Cordova !== 'undefined';
		}
	}
})();
