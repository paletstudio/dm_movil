(function() {
	'use strict';

	angular
		.module('main')
		.service('videoService', videoService);

	videoService.$inject = [
		'$q',
		'$timeout',
		'$ionicPlatform',
		'$ionicActionSheet',
		'$cordovaCapture',
		'$cordovaCamera',
		'utilsService',
		'$cordovaFileTransfer',
		'sessionService',
		'programService',
		'Config'
	];

	/* @ngInject */
	function videoService($q, $timeout, $ionicPlatform, $ionicActionSheet, $cordovaCapture, $cordovaCamera, utilsService, $cordovaFileTransfer, sessionService, programService, Config) {
		var me = this;

		me.takeVideo = function() {
			return $q(function(resolve, reject) {
				$ionicActionSheet.show({
					titleText: 'Tomar video',
					buttons: [{
							text: '<i class="icon ion-ios-videocam"></i> ' + 'Cámara'
						},
						{
							text: '<i class="icon ion-images"></i> ' + 'Elegir de la biblioteca'
						}
					],
					cancelText: '<i class="icon ion-close"></i> ' + 'Cancelar',
					buttonClicked: function(index) {
						var deferred = $q.defer();
						if (index === 0) {
							recordVideo()
								.then(resolve)
								.catch(reject);
						} else {
							chooseVideo()
								.then(resolve)
								.catch(reject);
						}
						return true;
					}
				});
			});
		};

		me.editVideo = editVideo;

		function recordVideo() {
			return utilsService.requestCameraPermission()
				.then(function() {
					var options = getCaptureOptions();

					if (!options) {
						return $q.reject('something went wrong');
					}

					return $cordovaCapture.captureVideo(options)
						.then(function(data) {
							return $timeout(function() {
								return data[0].fullPath;
							}, 300);
						});
				});

		}

		function chooseVideo() {
			if (!utilsService.cordovaIsDefined()) {
				return $q.reject('not in a device');
			}
			//$ionicPlatform.ready(function() {
			var options = getCameraOptions();

			if (!options) {
				return $q.reject('something went wrong');
			}
			return $cordovaCamera.getPicture(options)
				.then(function(path) {
					return $q.resolve('file://' + path);
				});
			//});
		}

		me.uploadVideo = function(userId, questionId, path) {
			var filename = path.split('/').pop();
			var videoType = path.split('/').pop().split('.').pop();
			var mime = 'video/3gpp';
			var percentage = 0;
			var backendPath = '';
			if (videoType === 'mp4') {
				mime = 'video/mp4';
			}
			var token;

			return sessionService.getToken()
				.then(function(value) {
					token = value;

					var options = {
						fileKey: 'video',
						fileName: filename,
						chunkedMode: true,
						mimeType: mime,
						params: {
							question_id: questionId,
							user_id: userId
						},
						headers: {
							Authorization: 'Bearer ' + token
						}
					};
					// cordova.plugin.pDialog.init({
					// 	progressStyle: 'HORIZONTAL',
					// 	cancelable: true,
					// 	title: 'Espere por favor',
					// 	message: 'Subiendo video...'
					// });

					backendPath = Config.ENV.API_URL + programService.getProgramUrl('module', true) + '/exam/video';

					return $cordovaFileTransfer.upload(backendPath, path, options)
						.then(function(result) {
							//	cordova.plugin.pDialog.dismiss();
							console.log(result);
							return $q.resolve(JSON.parse(result.response));
						}, function(err) {
                            console.log(err);
							//cordova.plugin.pDialog.dismiss();
							return $q.reject(err);
						}, function(progress) {
							percentage = progress.loaded / progress.total * 100;
							//	cordova.plugin.pDialog.setProgress(percentage);
						});
				});
		};

		function editVideo(path) {
			return $q(function(resolve, reject) {
				var filename = utilsService.generateUUID();

				if (!VideoEditor) {
					return false; //$q.reject();
				}

				cordova.plugin.pDialog.init({
					progressStyle: 'HORIZONTAL',
					cancelable: true,
					title: 'Please wait',
					message: 'Compressing video...'
				});

				VideoEditor.transcodeVideo(
					function(result) {
						cordova.plugin.pDialog.dismiss();
						resolve(result);
					},
					function(reason) {
						//console.log('Plugin not supported by this device.');
						cordova.plugin.pDialog.dismiss();
						resolve(path);
					}, {
						fileUri: path, // the path to the video on the device
						outputFileName: filename, // the file name for the transcoded video
						outputFileType: VideoEditorOptions.OutputFileType.MPEG4, // android is always mp4
						optimizeForNetworkUse: VideoEditorOptions.OptimizeForNetworkUse.YES, // ios only
						saveToLibrary: true, // optional, defaults to true
						deleteInputFile: false, // optional (android only), defaults to false
						maintainAspectRatio: true, // optional (ios only), defaults to true
						width: 1280, // optional, see note below on width and height
						height: 720, // optional, see notes below on width and height
						videoBitrate: 1000000, // optional, bitrate in bits, defaults to 1 megabit (1000000)
						fps: 24, // optional (android only), defaults to 24
						audioChannels: 2, // optional (ios only), number of audio channels, defaults to 2
						audioSampleRate: 44100, // optional (ios only), sample rate for the audio, defaults to 44100
						audioBitrate: 128000, // optional (ios only), audio bitrate for the video in bits, defaults to 128 kilobits (128000)
						progress: function(info) {
							cordova.plugin.pDialog.setProgress(info * 100);
						} // info will be a number from 0 to 100
					}
				);
			});
		}

		function getCaptureOptions() {
			if (typeof navigator.device.capture === 'undefined') {
				return null;
			}

			return {
				limit: 1,
				duration: 1800
			};
		}

		function getCameraOptions() {
			if (typeof Camera === 'undefined') {
				return null;
			}

			return {
				quality: Config.ENV.VIDEO.QUALITY,
				destinationType: $cordovaCamera.DestinationType.FILE_URI,
				sourceType: $cordovaCamera.PictureSourceType.PHOTOLIBRARY,
				mediaType: $cordovaCamera.MediaType.VIDEO
			};
		}

		function checkPath(path) {
			if (path.search('file://') === -1) {
				return 'file://' + path;
			}
			return path;
		}
	}
})();
