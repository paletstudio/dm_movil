(function() {
	'use strict';

	angular
		.module('main')
		.factory('API', API);

	API.$inject = ['$http', '$q', 'Config', 'utilsService', 'storageService'];

	/* @ngInject */
	function API($http, $q, Config, utilsService, storageService) {

		var baseUrl = Config.ENV.API_URL;
		// var

		function makeRequest(verb, uri, data, config) {
			return storageService.get('user')
				.then(function(res) {

					verb = verb.toLowerCase();

					//start with the uri
					var url = baseUrl + uri;

					//Logica metida momentaneamente para enviar id de USER_ID
					if (res) {
						url = utilsService.addQueryParam(url, 'user_id=' + res.id);
					}

					var httpArgs = [url];
					if (verb.match(/post|put|patch/)) {
						httpArgs.push(data);
					}
					var headers = {
						headers: {
							'Content-Type': 'application/json'
						}
					};

					if (angular.isObject(config)) {
						angular.merge(headers, config);
					}

					httpArgs.push(headers);

					return $http[verb].apply(null, httpArgs)
						.catch(function(err) {
							if (err.status === -1) {
								console.log('Sin conexión a los servicios');
								return $q.reject('Sin conexión');
							} else if (err.status === 401) {
								console.log('Unauthorized');
								if (err.data.msg) {
									return $q.reject(err.data.msg);
								}
								return $q.reject('Unauthorized');
							}
                            console.log(err);
                            return $q.reject(err.data ? err.data.msg : 'Se produjo un error');
						});
				});
		}

		var service = {
			get: function(uri, config) {
				return makeRequest('get', uri, null, config);
			},
			post: function(uri, data, config) {
				return makeRequest('post', uri, data, config);
			},
			put: function(uri, data, config) {
				return makeRequest('put', uri, data, config);
			},
			delete: function(uri) {
				return makeRequest('delete', uri);
			},
			patch: function(uri, data, config) {
				return makeRequest('patch', uri, data, config);
			}
		};

		return service;
	}
})();
