(function() {
	'use strict';

	angular
		.module('main')
		.service('cameraService', cameraService);

	cameraService.$inject = ['$q',  'Config', '$ionicActionSheet', '$cordovaCamera', 'utilsService'];

	/* @ngInject */
	function cameraService($q, Config, $ionicActionSheet, $cordovaCamera, utilsService) {
		var me = this;

		me.getMultipleImages = function(limit) {
			if (!window.imagePicker) {
				return $q.reject('Not a device');
			}
			var q = $q.defer();
			window.imagePicker.getPictures(
				function(results) {
					q.resolve(results);
				},
				function(error) {
					q.reject(error);
				}, {
					maximumImagesCount: limit || 10,
					quality: Config.ENV.CAMERA.QUALITY,
					width: Config.ENV.CAMERA.WIDTH,
					heigth: Config.ENV.CAMERA.HEIGHT
				}
			);
			return q.promise;
		};

		me.getPicture = function(titleText, width, height, quality) {
			var q = $q.defer();
			var options = {
				quality: quality || Config.ENV.CAMERA.QUALITY,
				destinationType: 0, //$cordovaCamera.DestinationType.DATA_URL,
				sourceType: 1,
				allowEdit: false,
				encodingType: 0, //$cordovaCamera.EncodingType.JPEG,
				targetWidth: width || Config.ENV.CAMERA.WIDTH,
				targetHeight: height || Config.ENV.CAMERA.HEIGHT,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};

			function showActionSheet(callback) {

				$ionicActionSheet.show({
					titleText: titleText || 'Elegir imagen',
					buttons: [{
							text: '<i class="icon ion-camera"></i> ' + 'Cámara'
						},
						{
							text: '<i class="icon ion-images"></i> ' + 'Elegir de la biblioteca'
						}
					],
					cancelText: '<i class="icon ion-close"></i> ' + 'Cancelar',
					cancel: function() {
						q.reject('Cancelled');
					},
					buttonClicked: function(index) {
						if (index === 0) {
							angular.extend(options, {
								sourceType: 1,
								saveToPhotoAlbum: true,
							});
						} else if (index === 1) {
							angular.extend(options, {
								sourceType: 0
							});
						}
						if (callback && typeof(callback) === 'function') {
							callback();
						}
						return true;
					}
				});
			}

			showActionSheet(function() {
				if (!utilsService.cordovaIsDefined()) {
					q.reject('Not a device');
				}
				if (typeof navigator.camera === 'undefined') {
					q.reject('Camera plugin not instaled');
				}

				utilsService.requestCameraPermission()
					.then(function() {
						$cordovaCamera.getPicture(options)
							.then(q.resolve)
							.catch(q.reject);
					})
					.catch(q.reject);
			});

			return q.promise;
		};
	}
})();
