(function() {
	'use strict';

	angular
		.module('main')
		.filter('moduleName', moduleName);

	function moduleName() {
		return moduleNameFilter;

		function moduleNameFilter(params) {
			if (params && angular.isString(params)) {
				return params.replace(/(Modulo|Módulo)(\s)?(\d+)(\.)?(\:)?/, '').trim();
			}
			return params;
		}
	}
})();
