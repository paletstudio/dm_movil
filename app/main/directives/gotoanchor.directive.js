(function() {
	'use strict';

	angular
		.module('main')
		.directive('goToAnchor', goToAnchor);

	/* @ngInject */
	function goToAnchor() {
		var template = [
			'<div class="goToAnchor" style="position: absolute; left: 50%; top: auto; bottom: 4%; transform: translateX(-50%);">',
			'<button class="button button-clear button-icon chevron-down" style="color: white; font-size: 35px;"></button>',
			'</div>'
		].join('');
		var directive = {
			restrict: 'E',
			template: template,
			scope: {
				target: '@',
				delegate: '@'
			},
			controller: GoToAnchorController,
			controllerAs: 'vm',
			bindToController: true
		};

		return directive;

		function linkFunc(scope, el, attr, ctrl) {

		}
	}

	GoToAnchorController.$inject = ['$scope', '$location', '$ionicScrollDelegate', '$element'];

	/* @ngInject */
	function GoToAnchorController($scope, $location, $ionicScrollDelegate, $element) {
		var vm = this;

		function scrollTo() {
			if (vm.target && vm.delegate) {
				$location.hash(vm.target); //set the location hash
				var handle = $ionicScrollDelegate.$getByHandle(vm.delegate);
				handle.anchorScroll(true); // 'true' for animation
			}
		}

		$element.bind('click', function() {
			scrollTo();
		});
	}
})();
