(function() {
	'use strict';

	angular
		.module('main')
		.directive('buttonPdf', buttonPdf);

	/* @ngInject */
	buttonPdf.$inject = ['programService', 'utilsService', 'userService', '$window', 'sessionService', 'Config'];

	function buttonPdf(programService, utilsService, userService, $window, sessionService, Config) {
		var template = [
			'<button class="button button-dark text-uppercase">',
			'    <div class="row">',
			'        <div class="col-20">',
			'            <img ng-if="type === \'pdf\'" src="main/assets/images/pdf.png" width="50" alt="">',
			'            <i ng-if="type === \'email\'" class="icon ion-android-mail button-pdf-email-icon"></i>',
			'        </div>',
			'        <div class="col button-text">',
			'            <span>{{text ? text : \'O envíalo a tu correo electrónico\'}}</span>',
			'        </div>',
			'    </div>',
			'</button>'
		].join('');

		var directive = {
			restrict: 'E',
			template: template,
			link: linkFunc,
			scope: {
				content: '=',
				text: '@',
				type: '@'
			}
		};

		return directive;

		function linkFunc(scope, el, attr, ctrl) {
			scope.type = (scope.type === 'email' || scope.type === 'pdf') ? scope.type : 'pdf';
			var email = null;
			el.on('click', function() {
				programService.currentContent = scope.content.id;

				sessionService.getToken()
					.then(function(res) {
						console.log(res);
					});

				if (scope.type === 'email') {
					if (userService.getUser()) {
						email = userService.getUser().email;
					}

					programService.sendPDF2Email({
							email: email
						})
						.then(function(res) {
							utilsService.showToast(res.msg, 'long');
						});
				} else {
					sessionService.getToken()
						.then(function(res) {
							console.log(res);
							if (res) {
								var url = Config.ENV.API_URL + programService.getProgramUrl('content', true) + '/pdf?token=' + res;
								if (ionic.Platform.isAndroid()) {
									$window.open('https://docs.google.com/viewer?url=' + url, '_blank', 'location=yes');
								} else {
									$window.open(url, '_blank', 'location=no,enableViewportScale=true,toolbarposition=top,closebuttoncaption=Cerrar');
								}
							}
						});
				}
			});
		}
	}
})();
