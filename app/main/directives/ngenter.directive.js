(function() {
	'use strict';

	angular
		.module('main')
		.directive('ngEnter', directive);

	/* @ngInject */
	function directive() {
		return {
			link: function(scope, elements, attrs) {
				elements.bind('keypress', function(event) {
					if (event.which === 13) {
						scope.$apply(function() {
							scope.$eval(attrs.ngEnter);
						});
						event.preventDefault();
					}
				});
			}
		};
	}
})();
