/*eslint strict: [0]*/
function handleOpenURL(url) {
	var msg = '';
	setTimeout(function() {
		if (/changepass=true/.test(url)) {
			msg = 'La contraseña ha sido cambiada. Inicia sesión para poder cambiarla por una de tu preferencia.';
		}

		if (navigator && navigator.notification && msg) {
			navigator.notification.alert(msg, null, 'Hafaspot info');
		} else if (msg) {
			alert(msg);
		}
	}, 0);
}

(function() {
	'use strict';

	angular.module('main').run(runBlock);

	runBlock.$inject = [
		'$ionicPush',
		'$ionicPlatform',
		'$rootScope',
		'sessionService',
		'$state',
		'authManager',
		'cloudNotificationService',
		'storageService'
	];
	/* @ngInject */
	function runBlock($ionicPush, $ionicPlatform, $rootScope, sessionService, $state, authManager, cloudNotificationService, storageService) {
		// $ionicPush.register().then(function(t) {
		// 	return $ionicPush.saveToken(t);
		// }).then(function(t) {
		// 	console.log('Token saved:', t.token);
		// });

		$ionicPlatform.ready(function() {
			storageService.put('forzedLogOut', true);
			var notificationOpenedCallback = function(jsonData) {
				cloudNotificationService.dispatch(jsonData, 'opened');
			};

			var notificationReceivedCallback = function(jsonData) {
				cloudNotificationService.dispatch(jsonData, 'received');
			};

			window.plugins.OneSignal.startInit('95c3a1cf-1d61-416e-8dd1-346eb23ba7ba')
			.handleNotificationOpened(notificationOpenedCallback, 'opened')
			.handleNotificationReceived(notificationReceivedCallback, 'received')
			.inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.None)
			.endInit();

			window.plugins.OneSignal.getPermissionSubscriptionState(function(status) {
				storageService.put('playerId', status.subscriptionStatus.userId).then(function(res) {
					console.log(res);
					console.log('Se guardó el token del usuario');
				});
			});

			storageService.get('forzedLogOut').then(function(res) {
				if (res) {
					sessionService.logout().then(function(res) {
						console.log(res);
						storageService.put('forzedLogOut', false);
					});
				}
			});
		});


		authManager.checkAuthOnRefresh();
		authManager.redirectWhenUnauthenticated();

		$rootScope.$on('$stateChangeStart', function(event, toState) {

			sessionService.isLoggedIn().then(function(value) {
				if (toState.name !== 'login' && !value) {
					if (toState.name !== 'forgot') {
						event.preventDefault();
						$state.go('login');
						return;
					}
				}
			});
		});
	}
})();
