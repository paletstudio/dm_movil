(function() {
	'use strict';

	angular.module('main', [
		'ionic',
		'ngCordova',
		'ui.router',
		'ionic.native',
		'angular-locker',
		'angular-jwt',
		'ionic.cloud'
		// TODO: load other modules selected during generation
	]);

})();
